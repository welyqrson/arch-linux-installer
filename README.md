# arch-linux-installer
Installs a simple Arch Linux system on a single disk.
## Usage
After booting into the Arch Linux live environment, download and run the
installation script by running
```shell
# curl https://gitlab.com/isobering/arch-linux-installer/raw/master/arch-install > arch-linux-installer
# chmod +x arch-linux-installer
# ./arch-linux-installer /dev/sda
```
For help and usage information, run
```shell
# ./arch-linux-installer --help
```
## How it Works
This script automates the installation process given in the
[Arch Linux Installation Guide](https://wiki.archlinux.org/index.php/Installation_guide).

This script deviates from the basic Arch Linux Installation Guide in two ways:

1. The root partition is encrypted using dm-crypt
2. The installation uses
   [Logical Volume Management (LVM)](https://wiki.archlinux.org/index.php/LVM)
   instead of regular partitions.

This script is not intended to be a general-purpose installation tool. Most
users will probably get more value out of following the Installation Guide or
writing their own automation script. Feel free to use this script as a starting
point!

### Installed Packages
In addition to the `base` and `base-devel` packages, the script installs the Gnome desktop
environment, proprietary NVIDIA graphics drivers, microcode for Intel CPUs, and
and several other useful packages:

- `bash-completion` - Programmable completion for the bash shell
- `firefox` - Standalone web browser from mozilla.org
- `util-linux` - Miscellaneous system utilities for Linux

These packages provide a basic Arch Linux system with a user-friendly desktop
environment.

### Installing on a VirtualBox VM
To install Arch Linux on a VirtualBox VM, use the `--virtualbox` argument:
```shell
# ./arch-linux-installer --virtualbox /dev/sda
```
The `--virtualbox` argument installs the `virtualbox-guest-utils` and
`virtualbox-guest-modules-arch` packages and prevents the script from
installing graphics drivers.

### Suspend and Hibernation
If the target system uses hibernation (suspend to disk), use the `--hibernate` argument:
```shell
# ./arch-linux-installer --hibernate /dev/sda
```
The script determines how much swap space the target system requires based on
the `--hibernate` argument and the total amount of available physical memory.
The swap size calculation is based on rules of thumb and worked examples
in the [Ubuntu SwapFAQ Community Help discussion](https://help.ubuntu.com/community/SwapFaq).
Please note that the Ubuntu article has some errors - the rules of thumb described
in the article will not produce the results presented in the worked examples.

For systems with < 1 GB of physical memory:

- With `--hibernate`, swap size is set equal to twice the amount of RAM
- Otherwise, swap size is set equal to the amount of RAM

For systems with > 1 GB of physical memory:

- With `--hibernate`, swap size is set equal to RAM + round(sqrt(RAM))
- Otherwise, swap size is set equal to round(sqrt(RAM))

These rules of thumb may be overly conservative for systems that use hibernation.
See [the Arch Linux Wiki article on swap](https://wiki.archlinux.org/index.php/Swap)
for a discussion on how swap works and how to improve swap performance.